import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node {

    private static int idCounter = 0;
    private int id;

    //public double distance = Double.MAX_VALUE;
    public Coordinates coordinates;

    public List<Node> neighborNodes = new ArrayList<>();

    public Node(int x, int y) {
        this.id = idCounter++;
        this.coordinates = new Coordinates(x, y);
    }

    public void addDestination(Node destination) {
        neighborNodes.add(destination);
    }

    public double calculateDistance(Node nodeB) {
        return Math.sqrt(Math.pow((nodeB.coordinates.x - this.coordinates.x), 2) + Math.pow((nodeB.coordinates.y - this.coordinates.y),2));
    }

    private static class Coordinates {
        int x, y;
        public Coordinates(int x, int y) { this.x = x; this.y = y; }
    }
}
