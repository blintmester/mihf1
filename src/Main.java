import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static final DecimalFormat df = new DecimalFormat("0.00");

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int pointPairCount, nodeCount, edgeCounts;

        pointPairCount = scanner.nextInt();
        nodeCount = scanner.nextInt();
        edgeCounts = scanner.nextInt();

        List<PointPair> pointPairs = new ArrayList<PointPair>();
        List<Node> nodes = new ArrayList<Node>();

        for (int i = 0; i < pointPairCount; i++) {
            PointPair pointPair = new PointPair(scanner.nextInt(), scanner.nextInt());
            pointPairs.add(pointPair);
        }

        scanner.nextLine();

        for (int i = 0; i < nodeCount; i++) {
            Node node = new Node(scanner.nextInt(), scanner.nextInt());
            nodes.add(node);
        }

        for (int i = 0; i < edgeCounts; i++) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            nodes.get(a).addDestination(nodes.get(b));
            nodes.get(b).addDestination(nodes.get(a));
        }

        for (PointPair pointPair : pointPairs) {
            System.out.print(df.format(Dijkstra.shortestPath(nodes.get(pointPair.source), nodes.get(pointPair.destination))));
            System.out.print("\t");
        }
        //System.out.println("");
    }
}
