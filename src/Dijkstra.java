import java.util.*;

public class Dijkstra {
    private static Node nearestNode(HashSet<Node> nodes, HashMap<Node, Double> distances) {
        return nodes.stream().min(Comparator.comparingDouble(distances::get)).orElse(null);
    }

    public static double shortestPath(Node source, Node destination) {
        HashSet<Node> settled = new HashSet<>();
        HashSet<Node> unsettled = new HashSet<>();
        HashMap<Node, Double> distances = new HashMap<>();

        unsettled.add(source);
        distances.put(source, 0.0);

        while (unsettled.size() != 0) {
            Node current = nearestNode(unsettled, distances);
            unsettled.remove(current);

            current.neighborNodes.forEach((neighbour) -> {
                double distance = current.calculateDistance(neighbour);
                if (!settled.contains(neighbour)) {
                    if (distances.get(current) + distance < distances.getOrDefault(neighbour, Double.MAX_VALUE)) {
                        distances.put(neighbour, distances.get(current) + distance);
                    }
                    unsettled.add(neighbour);
                }
            });
            settled.add(current);
            if (current == destination) {
                return distances.get(current);
            }
        }
        return Double.MAX_VALUE;
    }
}
