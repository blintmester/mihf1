public class PointPair {
    public int source, destination;

    public PointPair(int source, int destination) {
        this.source = source;
        this.destination = destination;
    }
}
